package com.atlassian.plugin.automation.core.auditlog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class EventAuditMessageBuilder {
    private Date timestamp = new Date();
    private String actor = null;
    private int ruleId;
    private String message;
    private String triggerMessage = "";
    private List<String> actionMessages = new ArrayList<String>();
    private String errors = "";

    public EventAuditMessageBuilder setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public EventAuditMessageBuilder setActor(final String actor) {
        this.actor = actor;
        return this;
    }

    public EventAuditMessageBuilder setRuleId(final int ruleId) {
        this.ruleId = ruleId;
        return this;
    }

    public EventAuditMessageBuilder setMessage(final String message) {
        this.message = message;
        return this;
    }

    public EventAuditMessageBuilder setTriggerMessage(final String triggerMessage) {
        this.triggerMessage = triggerMessage;
        return this;
    }

    public EventAuditMessageBuilder setActionMessages(final Iterable<String> actionMessages) {
        this.actionMessages = new ArrayList<String>();
        for (String message : actionMessages) {
            this.actionMessages.add(message);
        }
        return this;
    }

    public EventAuditMessageBuilder setErrors(final String errors) {
        this.errors = errors;
        return this;
    }

    public DefaultEventAuditMessage build() {
        return new DefaultEventAuditMessage(timestamp, actor, ruleId, message, triggerMessage, actionMessages, errors);
    }
}