package com.atlassian.plugin.automation.core;

/**
 * Represents triggers scheduled to execute at certain times.
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface CronTrigger<T> extends Trigger<T>
{
    /**
     * @return The CRON string required to schedule a CronTrigger
     */
    String getCronString();
}
