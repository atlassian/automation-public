package com.atlassian.plugin.automation.core;

import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Abstract baseclass that cron based triggers can implement.
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public abstract class AbstractCronTrigger<T> implements CronTrigger<T>
{
    private static final String CRON_EXPRESSION = "cronExpression";

    private String cronExpression;

    @Override
    public String getCronString()
    {
        return cronExpression;
    }

    @Override
    public void init(final TriggerConfiguration config)
    {
        cronExpression = singleValue(config, "cronExpression");
    }

    @Override
    public ErrorCollection validateAddConfiguration(final I18nResolver i18n, final Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        final String cronString = singleValue(params, CRON_EXPRESSION);
        if (!isValidCronExpression(cronString))
        {
            errors.addError("cronExpression", i18n.getText("rule.trigger.cron.invalid"));
        }

        return errors;
    }

    protected abstract boolean isValidCronExpression(final String cronExpression);
}
