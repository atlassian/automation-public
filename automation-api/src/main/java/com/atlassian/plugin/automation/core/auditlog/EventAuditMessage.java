package com.atlassian.plugin.automation.core.auditlog;

/**
 * This represents one single line in the audit log
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface EventAuditMessage extends AuditMessage
{
    /**
     * @return returns a trigger specific message {@link com.atlassian.plugin.automation.core.Trigger#getAuditLog()}
     */
    String getTriggerMessage();

    /**
     * @return returns action specific messages {@link com.atlassian.plugin.automation.core.Action#getAuditLog()}
     */
    Iterable<String> getActionMessages();

    /**
     * The errors that have occurred
     */
    String getErrors();
}
