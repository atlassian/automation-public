package com.atlassian.plugin.automation.core;

import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;

/**
 * Represents a trigger that when executed will calculate a list of items to which actions should be applied.
 * <p/>
 * Items could be JIRA issues, confluence pages or anything else that makes sense.
 * <p/>
 * NOTE: Implementations should not implement this directly but instead implement one of {@link CronTrigger} or {@link
 * EventTrigger}.
 *
 * @param <T> the type of item to return. Could be a JIRA Issue or Confluence page for example
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface Trigger<T>
{
    /**
     * Init the trigger
     */
    void init(TriggerConfiguration config);

    /**
     * Return a list of items to apply actions to.  Cannot be null.
     *
     * @param context the context to be passed to the trigger
     * @return a list of items to apply actions to or an empty list if there's no actions to carry out.
     */
    Iterable<T> getItems(TriggerContext context, ErrorCollection errorCollection);

    /**
     * Returns audit log string for this trigger (Can include pretty trigger name, params)
     */
    AuditString getAuditLog();

    /**
     * Returns the HTML portion specific to this trigger needed to configure it. The HTML will be embedded in an
     * existing <a href="https://developer.atlassian.com/display/AUI/Forms">AUI form</a>. Plugin developers should not
     * wrap the HTML in a &lt;form/&gt; tag but instead wrap individual form fields in &lt;div class="field-group"/&gt;
     * tags.
     *
     * @param triggerConfiguration The current config options for this trigger, which may be used to pre-populate form
     *                            fields. If null, default configuration is shown
     * @param actor  Actor under which this action shall be performed (might be used for access rights check)
     * @return the rendered HTML config form for this trigger
     */
    String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, String actor);

    /**
     * Returns the HTML portion specific to this trigger needed to display it's curent configuraiton.
     *
     * @param triggerConfiguration The current config options for this trigger, which may be used to pre-populate form
     *                            fields. If null, default configuration is shown
     * @param actor  Actor under which this action shall be performed (might be used for access rights check)
     * @return the rendered HTML view for this trigger
     */
    String getViewTemplate(TriggerConfiguration triggerConfiguration, String actor);

    /**
     * Given a map of form parameters submitted by the user this method should validate the params for this trigger
     *
     * @param i18n I18nResolver needed to internationalize error messages
     * @param params User submitted parameters for validation
     * @param actor Actor under which this action shall be performed (might be used for access rights check)
     * @return an error collection containing all validation errors
     */
    ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor);
}

