package com.atlassian.plugin.automation.core.codebarrel;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Represents an automation rule configuration including all configured components in this rule.
 * <p>
 * <ul>
 * <li>{@link #state} can be one of "NEW", "ENABLED", "DISABLED"</li>
 * <li>{@link #notifyOnError} can be one of "FIRSTERROR", "EVERYERROR", "NEVER"</li>
 * <li>{@link #associatedProjects} will be empty for global rules, or contain the projectIds for project specific rules</li>
 * </ul>
 *
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class RuleConfig {
    private Long id;
    private String name;
    private String state;
    private String description;
    private boolean canOtherRuleTrigger;
    private String notifyOnError;
    private String authorKey;
    private String actorKey;
    private Date created;
    private Date updated;
    private ComponentConfig trigger;
    private List<ComponentConfig> components = Lists.newArrayList();
    private Set<Long> associatedProjects = Sets.newHashSet();

    public RuleConfig(Long id,
                      String name,
                      String state,
                      String description,
                      boolean canOtherRuleTrigger,
                      String notifyOnError,
                      String authorKey,
                      String actorKey,
                      Date created,
                      Date updated,
                      ComponentConfig trigger,
                      List<ComponentConfig> components,
                      Set<Long> associatedProjects) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.description = description;
        this.canOtherRuleTrigger = canOtherRuleTrigger;
        this.notifyOnError = notifyOnError;
        this.authorKey = authorKey;
        this.actorKey = actorKey;
        this.created = created;
        this.updated = updated;
        this.trigger = trigger;
        this.components = components;
        this.associatedProjects = associatedProjects;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public String getDescription() {
        return description;
    }

    public boolean isCanOtherRuleTrigger() {
        return canOtherRuleTrigger;
    }

    public String getNotifyOnError() {
        return notifyOnError;
    }

    public String getAuthorKey() {
        return authorKey;
    }

    public String getActorKey() {
        return actorKey;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public ComponentConfig getTrigger() {
        return trigger;
    }

    public List<ComponentConfig> getComponents() {
        return components;
    }

    public Set<Long> getAssociatedProjects() {
        return associatedProjects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RuleConfig)) return false;
        RuleConfig that = (RuleConfig) o;
        return isCanOtherRuleTrigger() == that.isCanOtherRuleTrigger() &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getState(), that.getState()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getNotifyOnError(), that.getNotifyOnError()) &&
                Objects.equals(getAuthorKey(), that.getAuthorKey()) &&
                Objects.equals(getActorKey(), that.getActorKey()) &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getUpdated(), that.getUpdated()) &&
                Objects.equals(getTrigger(), that.getTrigger()) &&
                Objects.equals(getComponents(), that.getComponents()) &&
                Objects.equals(getAssociatedProjects(), that.getAssociatedProjects());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getState(), getDescription(), isCanOtherRuleTrigger(), getNotifyOnError(), getAuthorKey(), getActorKey(), getCreated(), getUpdated(), getTrigger(), getComponents(), getAssociatedProjects());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RuleConfig{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", canOtherRuleTrigger=").append(canOtherRuleTrigger);
        sb.append(", notifyOnError='").append(notifyOnError).append('\'');
        sb.append(", authorKey='").append(authorKey).append('\'');
        sb.append(", actorKey='").append(actorKey).append('\'');
        sb.append(", created=").append(created);
        sb.append(", updated=").append(updated);
        sb.append(", trigger=").append(trigger);
        sb.append(", components=").append(components);
        sb.append(", associatedProjects=").append(associatedProjects);
        sb.append('}');
        return sb.toString();
    }
}
