package com.atlassian.plugin.automation.core.codebarrel;


import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;

/**
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface ValidationContext {
    String getActor();

    I18nResolver getI18n();

    RuleConfig getRuleConfig();

    Map<String, List<String>> getParams();
}
