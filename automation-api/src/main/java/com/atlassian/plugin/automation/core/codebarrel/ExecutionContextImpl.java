package com.atlassian.plugin.automation.core.codebarrel;

import com.atlassian.plugin.automation.util.ErrorCollection;

import java.util.Objects;

/**
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class ExecutionContextImpl<T> implements ExecutionContext<T> {
    private final String actor;
    private final RuleConfig ruleConfig;
    private final Iterable<T> items;
    private final ErrorCollection errorCollection;

    public ExecutionContextImpl(String actor, RuleConfig ruleConfig, Iterable<T> items, ErrorCollection errorCollection) {
        this.actor = actor;
        this.ruleConfig = ruleConfig;
        this.items = items;
        this.errorCollection = errorCollection;
    }

    @Override
    public String getActor() {
        return actor;
    }

    @Override
    public RuleConfig getRuleConfig() {
        return ruleConfig;
    }

    @Override
    public Iterable<T> getItems() {
        return items;
    }

    @Override
    public ErrorCollection getErrorCollection() {
        return errorCollection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExecutionContextImpl)) return false;
        ExecutionContextImpl<?> that = (ExecutionContextImpl<?>) o;
        return Objects.equals(getActor(), that.getActor()) &&
                Objects.equals(getRuleConfig(), that.getRuleConfig()) &&
                Objects.equals(getItems(), that.getItems()) &&
                Objects.equals(getErrorCollection(), that.getErrorCollection());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActor(), getRuleConfig(), getItems(), getErrorCollection());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExecutionContextImpl{");
        sb.append("actor='").append(actor).append('\'');
        sb.append(", config=").append(ruleConfig);
        sb.append(", items=").append(items);
        sb.append(", errorCollection=").append(errorCollection);
        sb.append('}');
        return sb.toString();
    }
}
