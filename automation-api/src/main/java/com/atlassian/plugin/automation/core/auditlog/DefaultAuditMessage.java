package com.atlassian.plugin.automation.core.auditlog;

import java.util.Date;

/**
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class DefaultAuditMessage implements AuditMessage {
    private final Date timestamp;
    private final String actor;
    private final int ruleId;
    private final String message;

    public DefaultAuditMessage(final Date timestamp, final String actor, final int ruleId, final String message) {
        this.timestamp = timestamp;
        this.actor = actor;
        this.ruleId = ruleId;
        this.message = message;
    }

    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String getActor() {
        return actor;
    }

    @Override
    public int getRuleId() {
        return ruleId;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "DefaultAuditMessage{" +
                "timestamp=" + timestamp +
                ", actor='" + actor + '\'' +
                ", ruleId=" + ruleId +
                ", message='" + message + '\'' +
                '}';
    }
}
