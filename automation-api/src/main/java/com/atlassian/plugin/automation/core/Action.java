package com.atlassian.plugin.automation.core;

import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;

/**
 * Represents an action that will be executed for all items.
 *
 * @param <T> the type of the item to apply the action to. Could be JIRA issues or Confluence pages for example
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface Action<T>
{
    /**
     * Init the action
     *
     * @param config the action configuration
     */
    void init(ActionConfiguration config);

    /**
     * Executes this action over the items provided. Will return an error collection with any potential errors.
     *
     * @param actor the user to execute the action as
     * @param items The items to apply this action to
     */
    void execute(String actor, Iterable<T> items, ErrorCollection errorCollection);

    /**
     * @return audit log for this action 
     */
    AuditString getAuditLog();

    /**
     * Returns the HTML portion specific to this action needed to configure it. The HTML will be
     * embedded in an existing <a href="https://developer.atlassian.com/display/AUI/Forms">AUI form</a>. Plugin
     * developers should not wrap the HTML in a &lt;form/&gt; tag but instead wrap individual form fields in &lt;div
     * class="field-group"/&gt; tags.
     *
     * @param actionConfiguration The current config options for this action, which may be used to pre-populate form
     *                            fields.  If null, default configuration is shown
     * @param actor  Actor under which this action shall be performed (might be used for access rights check)
     * @return the rendered HTML config form for this action
     */
    String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor);

    /**
     * Returns the HTML portion specific to this action needed to view it.
     *
     * @param actionConfiguration The current config options for this action, which will be used to render the view
     * @param actor  Actor under which this action shall be performed (might be used for access rights check)
     * @return the rendered HTML view for this action
     */
    String getViewTemplate(ActionConfiguration actionConfiguration, String actor);

    /**
     * Given a map of form parameters submitted by the user this method should validate the params for this action
     *
     * @param i18n   I18nResolver needed to internationalize error messages
     * @param params User submitted parameters for validation
     * @param actor  Actor under which this action shall be performed (might be used for access rights check)
     * @return an error collection containing all validation errors
     */
    ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor);
}
