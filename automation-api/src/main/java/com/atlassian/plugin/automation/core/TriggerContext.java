package com.atlassian.plugin.automation.core;

/**
 * Contains context data for the trigger. Depending on the source, the sourceEvent might not be filled
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class TriggerContext
{
    private final String actor;
    private final Object sourceEvent;

    public TriggerContext(String actor)
    {
        this.sourceEvent = null;
        this.actor = actor;
    }

    public TriggerContext(String actor, Object sourceEvent)
    {
        this.actor = actor;
        this.sourceEvent = sourceEvent;
    }

    public Object getSourceEvent()
    {
        return sourceEvent;
    }

    public String getActor()
    {
        return actor;
    }
}
