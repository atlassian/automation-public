package com.atlassian.plugin.automation.core.codebarrel;


import com.atlassian.plugin.automation.util.ErrorCollection;

/**
 * Contains all parameters to execute a rule
 *
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface ExecutionContext<T> {
    String getActor();

    RuleConfig getRuleConfig();

    Iterable<T> getItems();

    ErrorCollection getErrorCollection();
}
