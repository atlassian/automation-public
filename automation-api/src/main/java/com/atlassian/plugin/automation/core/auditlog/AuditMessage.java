package com.atlassian.plugin.automation.core.auditlog;

import java.util.Date;

/**
 * This represents one single line in the admin audit log
 *
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface AuditMessage {
    /**
     * Timestamp of the message
     */
    Date getTimestamp();

    /**
     * Actor of the rule
     */
    String getActor();

    /**
     * Id of the rule
     */
    int getRuleId();

    /**
     * Returns the message to be shown
     */
    String getMessage();

}
