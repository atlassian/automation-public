package com.atlassian.plugin.automation.core.codebarrel;


import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Individual automation rule component.
 * <p>
 * <ul>
 * <li>{@link #component} can be one of "TRIGGER", "CONDITION" or "ACTION"</li>
 * <li>{@link #type} will be the more detailed sub-type (e.g. 'jira.jql.scheduled')</li>
 * </ul>
 * <p>
 * Components can also contain children, which is how rule branches are represented.
 *
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class ComponentConfig {
    private String id;
    private String component;
    private String type;
    private Map<String, Object> value;
    private String parentId;
    private List<ComponentConfig> children = Lists.newArrayList();

    public ComponentConfig(String id, String component, String parentId, String type, Map<String, Object> value, List<ComponentConfig> children) {
        this.id = id;
        this.component = component;
        this.parentId = parentId;
        this.type = type;
        this.value = value;
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public String getComponent() {
        return component;
    }

    public String getParentId() {
        return parentId;
    }

    public String getType() {
        return type;
    }

    public Map<String, Object> getValue() {
        return value;
    }

    public List<ComponentConfig> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComponentConfig)) return false;
        ComponentConfig that = (ComponentConfig) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getComponent(), that.getComponent()) &&
                Objects.equals(getParentId(), that.getParentId()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getValue(), that.getValue()) &&
                Objects.equals(getChildren(), that.getChildren());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getComponent(), getParentId(), getType(), getValue(), getChildren());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComponentConfig{");
        sb.append("id='").append(id).append('\'');
        sb.append(", component='").append(component).append('\'');
        sb.append(", parentId='").append(parentId).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", value=").append(value);
        sb.append(", children=").append(children);
        sb.append('}');
        return sb.toString();
    }
}
