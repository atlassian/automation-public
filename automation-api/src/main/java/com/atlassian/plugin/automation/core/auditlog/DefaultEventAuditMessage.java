package com.atlassian.plugin.automation.core.auditlog;

import java.util.Date;

/**
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class DefaultEventAuditMessage extends DefaultAuditMessage implements EventAuditMessage {
    private final String triggerMessage;
    private final Iterable<String> actionMessages;
    private final String errors;

    DefaultEventAuditMessage(final Date timestamp, final String actor, final int ruleId, final String message,
                             final String triggerMessage, final Iterable<String> actionMessages, final String errors) {
        super(timestamp, actor, ruleId, message);
        this.triggerMessage = triggerMessage;
        this.actionMessages = actionMessages;
        this.errors = errors;
    }

    @Override
    public String getTriggerMessage() {
        return triggerMessage;
    }

    @Override
    public Iterable<String> getActionMessages() {
        return actionMessages;
    }

    @Override
    public String getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "DefaultEventAuditMessage{" +
                "timestamp=" + getTimestamp() +
                ", actor='" + getActor() + '\'' +
                ", ruleId=" + getRuleId() +
                ", message='" + getMessage() + '\'' +
                ", triggerMessage='" + triggerMessage + '\'' +
                ", actionMessages=" + actionMessages +
                ", errors='" + errors + '\'' +
                '}';
    }
}
