package com.atlassian.plugin.automation.core.action;

import com.atlassian.plugin.automation.core.AutomationConfiguration;

/**
 * Marker interface for action configuration
 *
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface ActionConfiguration extends AutomationConfiguration
{
}
