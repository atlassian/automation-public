package com.atlassian.plugin.automation.core.codebarrel;


import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;

/**
 * This interface can be implemented by third party extensions that wish to receive the rule configuration (so they can lookup, the JQL
 * filter of the trigger for example).  If an action implements this interface, *only* the
 * {@link #execute(ExecutionContext)} execute method here will be invoked.
 * {@link Action#execute(String, Iterable, ErrorCollection)} will *not* be called.
 * <p>
 * The same applies to validate. We'll only call {@link #validateConfiguration(ValidationContext)}  instead of the old
 * {@link #validateAddConfiguration(I18nResolver, Map, String)}.
 * <p>
 * This will only be called by Automation for JIRA 3.1.0 and above.
 *
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface RuleConfigAwareAction<T> extends Action<T> {
    /*
    This should replace the deprecated methods below when we start supporting JDK 8.

    @Override
    @Deprecated
    default void execute(String actor, Iterable<T> items, ErrorCollection errorCollection) {
        throw new UnsupportedOperationException("Superseded by execute(ExecutionContext) below");
    }

    @Override
    @Deprecated
    default ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor) {
        throw new UnsupportedOperationException("Superseded by validateConfiguration(ValidationContext) below");
    }

    @Override
    @Deprecated
    default String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor) {
        throw new UnsupportedOperationException("This method should no longer be called in the latest Automation for JIRA.");
    }

    @Override
    @Deprecated
    default String getViewTemplate(ActionConfiguration actionConfiguration, String actor) {
        throw new UnsupportedOperationException("This method should no longer be called in the latest Automation for JIRA.");
    }
    */

    /**
     * @deprecated as of 2.1.0 implementations should implement {@link #execute(ExecutionContext)} instead
     */
    @Override
    @Deprecated
    void execute(String actor, Iterable<T> items, ErrorCollection errorCollection);

    /**
     * @deprecated as of 2.1.0 implementations should implement {@link #validateConfiguration(ValidationContext)} instead
     */
    @Override
    @Deprecated
    ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor);

    /**
     * @deprecated This method will no longer be called in Automation for JIRA 3.0.0 or later. See https://bitbucket.org/codebarrel/automation-addon-sample/
     */
    @Override
    @Deprecated
    String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor);

    /**
     * @deprecated This method will no longer be called in Automation for JIRA 3.0.0 or later. See https://bitbucket.org/codebarrel/automation-addon-sample/
     */
    @Override
    @Deprecated
    String getViewTemplate(ActionConfiguration actionConfiguration, String actor);

    /**
     * Performs the action
     *
     * @param executionContext contains all parameters needed for execution
     */
    void execute(ExecutionContext<T> executionContext);

    /**
     * Validates the configuration for this action.
     *
     * @param validationContext contains all parameters needed for validation
     * @return Any errors if invalid
     */
    ErrorCollection validateConfiguration(ValidationContext validationContext);
}
