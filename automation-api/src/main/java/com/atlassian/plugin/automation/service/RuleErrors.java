package com.atlassian.plugin.automation.service;

import com.atlassian.plugin.automation.util.ErrorCollection;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents all validation errors that may occur for a rule.
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class RuleErrors
{
    @JsonProperty
    private final ErrorCollection ruleErrors;
    @JsonProperty
    private final ErrorCollection triggerErrors;
    @JsonProperty
    private final List<ErrorCollection> actionErrors;

    public RuleErrors(ErrorCollection ruleErrors)
    {
        this(ruleErrors, new ErrorCollection(), new ArrayList<ErrorCollection>());
    }

    public RuleErrors(final ErrorCollection ruleErrors, final ErrorCollection triggerErrors, final List<ErrorCollection> actionErrors)
    {
        this.ruleErrors = ruleErrors;
        this.triggerErrors = triggerErrors;
        this.actionErrors = actionErrors;
    }

    public ErrorCollection getRuleErrors()
    {
        return ruleErrors;
    }

    public ErrorCollection getTriggerErrors()
    {
        return triggerErrors;
    }

    public List<ErrorCollection> getActionErrors()
    {
        return actionErrors;
    }

    public boolean hasAnyErrors()
    {
        if (ruleErrors.hasAnyErrors() || triggerErrors.hasAnyErrors())
        {
            return true;
        }
        for (ErrorCollection actionError : actionErrors)
        {
            if (actionError.hasAnyErrors())
            {
                return true;
            }
        }
        return false;
    }
}
