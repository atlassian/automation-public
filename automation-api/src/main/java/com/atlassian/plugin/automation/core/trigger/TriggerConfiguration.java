package com.atlassian.plugin.automation.core.trigger;

import com.atlassian.plugin.automation.core.AutomationConfiguration;

/**
 * Trigger configuration extending AutomationConfiguration
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public interface TriggerConfiguration extends AutomationConfiguration
{
}
