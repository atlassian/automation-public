package com.atlassian.plugin.automation.core.auditlog;

/**
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class DefaultAuditString implements AuditString {
    private final String log;

    public DefaultAuditString(final String log) {
        this.log = log;
    }

    @Override
    public String getString() {
        return log;
    }
}
