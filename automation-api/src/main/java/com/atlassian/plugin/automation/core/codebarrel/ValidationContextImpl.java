package com.atlassian.plugin.automation.core.codebarrel;

import com.atlassian.sal.api.message.I18nResolver;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @since 2.1.0
 * @deprecated Please use the new thirdparty-api going forward.
 */
@Deprecated
public class ValidationContextImpl implements ValidationContext {
    private final String actor;
    private final I18nResolver i18n;
    private final RuleConfig ruleConfig;
    private final Map<String, List<String>> params;

    public ValidationContextImpl(String actor, I18nResolver i18n, final RuleConfig ruleConfig, Map<String, List<String>> params) {
        this.actor = actor;
        this.i18n = i18n;
        this.ruleConfig = ruleConfig;
        this.params = params;
    }

    @Override
    public String getActor() {
        return actor;
    }

    @Override
    public I18nResolver getI18n() {
        return i18n;
    }

    @Override
    public RuleConfig getRuleConfig() {
        return ruleConfig;
    }

    @Override
    public Map<String, List<String>> getParams() {
        return params;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValidationContextImpl)) return false;
        ValidationContextImpl that = (ValidationContextImpl) o;
        return Objects.equals(getActor(), that.getActor()) &&
                Objects.equals(getI18n(), that.getI18n()) &&
                Objects.equals(getRuleConfig(), that.getRuleConfig()) &&
                Objects.equals(getParams(), that.getParams());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActor(), getI18n(), getRuleConfig(), getParams());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ValidationContextImpl{");
        sb.append("actor='").append(actor).append('\'');
        sb.append(", i18n=").append(i18n);
        sb.append(", ruleConfig=").append(ruleConfig);
        sb.append(", params=").append(params);
        sb.append('}');
        return sb.toString();
    }
}
