package com.codebarrel.automation.api.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Stores all configuration related to a particular automation rule.
 */
@JsonAutoDetect
@org.codehaus.jackson.annotate.JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class RuleConfigBean {
    private Long id;
    private String clientKey;
    private String name;
    private RuleState state = RuleState.NEW;
    private String description;
    private boolean canOtherRuleTrigger;
    private NotifyOnError notifyOnError;
    private String authorAccountId;
    private String actorAccountId;
    private Date created;
    private Date updated;
    private ComponentConfigBean trigger;
    private List<ComponentConfigBean> components = Lists.newArrayList();
    private List<ProjectAssociationBean> projects = Lists.newArrayList();
    private List<Long> labels = Lists.newArrayList();
    private Set<RuleTagBean> tags = Sets.newHashSet();

    public RuleConfigBean() {
    }

    private RuleConfigBean(final Long id,
                           final String clientKey,
                           final String name,
                           final RuleState state,
                           final String description,
                           final String authorAccountId,
                           final String actorAccountId,
                           final Date created,
                           final Date updated,
                           final ComponentConfigBean trigger,
                           final List<ComponentConfigBean> components,
                           final boolean canOtherRuleTrigger,
                           final NotifyOnError notifyOnError,
                           final List<ProjectAssociationBean> projects,
                           final List<Long> labels,
                           final Set<RuleTagBean> tags) {
        this.id = id;
        this.clientKey = clientKey;
        this.name = name;
        this.state = state;
        this.description = description;
        this.authorAccountId = authorAccountId;
        this.actorAccountId = actorAccountId;
        this.created = created;
        this.updated = updated;
        this.trigger = trigger;
        this.components = components;
        this.canOtherRuleTrigger = canOtherRuleTrigger;
        this.notifyOnError = notifyOnError;
        this.projects = projects;
        this.labels = labels;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public String getClientKey() {
        return clientKey;
    }

    public String getName() {
        return name;
    }

    public RuleState getState() {
        return state;
    }

    public String getDescription() {
        return description;
    }

    public boolean isCanOtherRuleTrigger() {
        return canOtherRuleTrigger;
    }

    public NotifyOnError getNotifyOnError() {
        return notifyOnError;
    }

    @Nullable
    public String getAuthorAccountId() {
        return authorAccountId;
    }

    public String getActorAccountId() {
        return actorAccountId;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public ComponentConfigBean getTrigger() {
        return trigger;
    }

    public List<ComponentConfigBean> getComponents() {
        return components;
    }

    public List<ProjectAssociationBean> getProjects() {
        return projects;
    }

    public List<Long> getLabels() {
        return labels;
    }

    public Set<RuleTagBean> getTags() {
        return tags;
    }

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    public List<ComponentConfigBean> getComponentsIncludingTrigger() {
        final List<ComponentConfigBean> ret = Lists.newArrayList(trigger);
        ret.addAll(components);
        return ret;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RuleConfigBean that = (RuleConfigBean) o;
        return isCanOtherRuleTrigger() == that.isCanOtherRuleTrigger() &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getClientKey(), that.getClientKey()) &&
                Objects.equals(getName(), that.getName()) &&
                getState() == that.getState() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getNotifyOnError() == that.getNotifyOnError() &&
                Objects.equals(getAuthorAccountId(), that.getAuthorAccountId()) &&
                Objects.equals(getActorAccountId(), that.getActorAccountId()) &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getUpdated(), that.getUpdated()) &&
                Objects.equals(getTrigger(), that.getTrigger()) &&
                Objects.equals(getComponents(), that.getComponents()) &&
                Objects.equals(getProjects(), that.getProjects()) &&
                Objects.equals(getLabels(), that.getLabels()) &&
                Objects.equals(getTags(), that.getTags());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClientKey(), getName(), getState(), getDescription(), isCanOtherRuleTrigger(), getNotifyOnError(), getAuthorAccountId(), getActorAccountId(), getCreated(), getUpdated(), getTrigger(), getComponents(), getProjects(), getLabels(), getTags());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RuleConfigBean{");
        sb.append("id=").append(id);
        sb.append(", clientKey='").append(clientKey).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", state=").append(state);
        sb.append(", description='").append(description).append('\'');
        sb.append(", canOtherRuleTrigger=").append(canOtherRuleTrigger);
        sb.append(", notifyOnError=").append(notifyOnError);
        sb.append(", authorAccountId='").append(authorAccountId).append('\'');
        sb.append(", actorAccountId='").append(actorAccountId).append('\'');
        sb.append(", created=").append(created);
        sb.append(", updated=").append(updated);
        sb.append(", trigger=").append(trigger);
        sb.append(", components=").append(components);
        sb.append(", projects=").append(projects);
        sb.append(", labels=").append(labels);
        sb.append(", tags=").append(tags);
        sb.append('}');
        return sb.toString();
    }

    public static class Builder {
        private Long id;
        private String clientKey;
        private String name;
        private RuleState state = RuleState.NEW;
        private NotifyOnError notifyOnError = NotifyOnError.FIRSTERROR;
        private boolean canOtherRuleTrigger;
        private String description;
        private String authorAccountId;
        private String actorAccountId;
        private Date created;
        private Date updated;
        private ComponentConfigBean trigger;
        private List<ComponentConfigBean> components = Lists.newArrayList();
        private List<ProjectAssociationBean> projects = Lists.newArrayList();
        private List<Long> labels = Lists.newArrayList();
        private Set<RuleTagBean> tags = Sets.newHashSet();

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setClientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public Builder setState(RuleState state) {
            this.state = state;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setCanOtherRuleTrigger(boolean canOtherRuleTrigger) {
            this.canOtherRuleTrigger = canOtherRuleTrigger;
            return this;
        }

        public Builder setNotifyOnError(NotifyOnError notifyOnError) {
            this.notifyOnError = notifyOnError;
            return this;
        }

        public Builder setAuthorAccountId(String authorAccountId) {
            this.authorAccountId = authorAccountId;
            return this;
        }

        public Builder setActorAccountId(String actorAccountId) {
            this.actorAccountId = actorAccountId;
            return this;
        }

        public Builder setCreated(Date created) {
            this.created = created;
            return this;
        }

        public Builder setUpdated(Date updated) {
            this.updated = updated;
            return this;
        }

        public Builder setTrigger(ComponentConfigBean trigger) {
            this.trigger = trigger;
            return this;
        }

        public Builder setComponents(List<ComponentConfigBean> components) {
            this.components = components;
            return this;
        }

        public Builder setProjects(List<ProjectAssociationBean> projects) {
            this.projects = projects;
            return this;
        }

        public Builder setLabels(List<Long> labels) {
            this.labels = labels;
            return this;
        }

        public Builder setTags(Set<RuleTagBean> tags) {
            this.tags = tags;
            return this;
        }

        public Builder setRuleConfigBean(RuleConfigBean bean) {
            this.id = bean.getId();
            this.clientKey = bean.getClientKey();
            this.name = bean.getName();
            this.canOtherRuleTrigger = bean.canOtherRuleTrigger;
            this.notifyOnError = bean.getNotifyOnError();
            this.state = bean.getState();
            this.description = bean.getDescription();
            this.authorAccountId = bean.getAuthorAccountId();
            this.actorAccountId = bean.getActorAccountId();
            this.created = bean.getCreated();
            this.updated = bean.getUpdated();
            this.trigger = bean.getTrigger();
            this.components = Lists.newArrayList(bean.getComponents());
            this.projects = Lists.newArrayList(bean.getProjects());
            this.labels = bean.getLabels();
            this.tags = bean.getTags();
            return this;
        }

        public RuleConfigBean build() {
            // accountIds, default to user keys for server backwards compatibility.
            return new RuleConfigBean(id, clientKey, name, state, description, authorAccountId,
                    actorAccountId, created, updated, trigger, components,
                    canOtherRuleTrigger, notifyOnError, projects, labels, tags);
        }
    }
}
