package com.codebarrel.automation.api.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Stores the configuration of a rule component (e.g. an 'Add comment' action in a rule).
 */
@JsonAutoDetect
@org.codehaus.jackson.annotate.JsonAutoDetect
public class ComponentConfigBean {
    private String id;
    private ComponentType component;
    private String parentId;
    private String conditionParentId;
    private Integer schemaVersion;
    private String type;
    private Object value;
    private List<ComponentConfigBean> children = Lists.newArrayList();
    private List<ComponentConfigBean> conditions = Lists.newArrayList();
    @JsonIgnore
    private List<Long> optimisedIds = Lists.newArrayList();

    public ComponentConfigBean() {
    }

    private ComponentConfigBean(final String id,
                                final String parentId,
                                final String conditionParentId,
                                final ComponentType component,
                                final String type,
                                final Object value,
                                final List<ComponentConfigBean> conditions,
                                final List<ComponentConfigBean> children,
                                final Integer schemaVersion,
                                final Collection<Long> optimisedIds) {
        this.id = id;
        this.parentId = parentId;
        this.conditionParentId = conditionParentId;
        this.component = component;
        this.type = type;
        this.value = value;
        this.optimisedIds.addAll(optimisedIds);
        this.conditions.addAll(conditions);
        this.children.addAll(children);
        this.schemaVersion = schemaVersion;
    }

    public String getId() {
        return id;
    }

    public String getParentId() {
        return parentId;
    }

    public String getConditionParentId() {
        return conditionParentId;
    }

    public String getType() {
        return type;
    }

    public Integer getSchemaVersion() {
        return schemaVersion;
    }

    public ComponentType getComponent() {
        return component;
    }

    /**
     * Returns the config saved on front end via calls to window.CodeBarrel.Automation.updateComponentValue(config, newValue).
     * <p>
     * This will be returned as a Map by default so it's recommended to convert this to a stongly typed object.
     */
    public Object getValue() {
        return value;
    }

    public List<ComponentConfigBean> getConditions() {
        return conditions;
    }

    public List<ComponentConfigBean> getChildren() {
        return children;
    }

    /**
     * Shortcut method to get a config of a particular type.
     * Throws ClassCastException
     */
    @JsonIgnore
    public <T> T getValue(Class<T> clazz) {
        Object o = getValue();
        if (o != null) {
            if (clazz.isAssignableFrom(o.getClass())) {
                //noinspection unchecked
                return (T) o;
            } else {
                throw new ClassCastException("Unable to cast " + o + " to " + clazz);
            }
        } else {
            return null;
        }
    }

    @JsonIgnore
    public List<Long> getOptimisedIds() {
        return optimisedIds;
    }

    @JsonIgnore
    public boolean isNewComponent() {
        return StringUtils.isBlank(id) || id.startsWith("__NEW__");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComponentConfigBean{");
        sb.append("id='").append(id).append('\'');
        sb.append(", component=").append(component);
        sb.append(", parentId='").append(parentId).append('\'');
        sb.append(", conditionParentId='").append(conditionParentId).append('\'');
        sb.append(", schemaVersion=").append(schemaVersion);
        sb.append(", type='").append(type).append('\'');
        sb.append(", value=").append(value);
        sb.append(", conditions=").append(conditions);
        sb.append(", children=").append(children);
        sb.append(", optimisedIds=").append(optimisedIds);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComponentConfigBean)) return false;
        ComponentConfigBean that = (ComponentConfigBean) o;
        return Objects.equals(getId(), that.getId()) &&
                getComponent() == that.getComponent() &&
                Objects.equals(getParentId(), that.getParentId()) &&
                Objects.equals(getConditionParentId(), that.getConditionParentId()) &&
                Objects.equals(getSchemaVersion(), that.getSchemaVersion()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getValue(), that.getValue()) &&
                Objects.equals(getConditions(), that.getConditions()) &&
                Objects.equals(getChildren(), that.getChildren()) &&
                Objects.equals(getOptimisedIds(), that.getOptimisedIds());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getComponent(), getParentId(), getConditionParentId(), getSchemaVersion(), getType(), getValue(), getConditions(), getChildren(), getOptimisedIds());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String id;
        private ComponentType component;
        private String parentId;
        private String conditionParentId;
        private Integer schemaVersion = -1;
        private String type;
        private Object value;
        private List<ComponentConfigBean> conditions = Lists.newArrayList();
        private List<ComponentConfigBean> children = Lists.newArrayList();
        private Set<Long> optimisedIds = Sets.newLinkedHashSet();

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setComponent(ComponentType component) {
            this.component = component;
            return this;
        }

        public Builder setParentId(String parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder setConditionParentId(String conditionParentId) {
            this.conditionParentId = conditionParentId;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setValue(Object value) {
            this.value = value;
            return this;
        }

        public Builder setConditions(List<ComponentConfigBean> conditions) {
            this.conditions = Lists.newArrayList(conditions);
            return this;
        }

        public Builder setChildren(List<ComponentConfigBean> children) {
            this.children = Lists.newArrayList(children);
            return this;
        }

        public Builder setSchemaVersion(Integer schemaVersion) {
            this.schemaVersion = schemaVersion;
            return this;
        }

        public Builder setComponentConfigBean(ComponentConfigBean bean) {
            this.id = bean.getId();
            this.component = bean.getComponent();
            this.parentId = bean.getParentId();
            this.conditionParentId = bean.getConditionParentId();
            this.type = bean.getType();
            this.value = bean.getValue();
            this.children = Lists.newArrayList(bean.getChildren());
            this.conditions = Lists.newArrayList(bean.getConditions());
            this.optimisedIds = Sets.newLinkedHashSet(bean.getOptimisedIds());
            this.schemaVersion = bean.getSchemaVersion();
            return this;
        }

        public Builder addOptimisedId(String optimisedId) {
            if (NumberUtils.isNumber(optimisedId)) {
                this.optimisedIds.add(NumberUtils.toLong(optimisedId));
            }
            return this;
        }

        public ComponentConfigBean build() {
            return new ComponentConfigBean(id, parentId, conditionParentId, component, type, value, conditions, children, schemaVersion, optimisedIds);
        }
    }
}
