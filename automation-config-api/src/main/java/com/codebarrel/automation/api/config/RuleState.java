package com.codebarrel.automation.api.config;

/**
 * Different states a rule config can be in
 */
public enum RuleState {
    /**
     * Used for brand new rule configs that haven't been saved nor enabled yet.
     */
    NEW,
    /**
     * A saved enabled rule config
     */
    ENABLED,
    /**
     * A saved disabled rule config
     */
    DISABLED
}
