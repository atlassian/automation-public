package com.codebarrel.automation.api.config;


/**
 * Controls when error notifications should be sent for a particular rule.
 */
public enum NotifyOnError {
    /**
     * Only send out notifications the first time a rule switches from a SUCCESS status to an ERROR status.
     */
    FIRSTERROR,
    /**
     * Send notifications for every error.
     */
    EVERYERROR,
    /**
     * Send no error notifications.
     */
    NEVER
}
