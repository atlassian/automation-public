package com.codebarrel.automation.api.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

/**
 * A tag which can be added to a {@link RuleConfigBean}
 */
@JsonAutoDetect
@org.codehaus.jackson.annotate.JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class RuleTagBean {
    private Long id;
    private Long ruleId;
    private String tagType;
    private String tagValue;

    public RuleTagBean() {
    }

    private RuleTagBean(final Long id,
                        final Long ruleId,
                        final String tagType,
                        final String tagValue) {
        this.id = id;
        this.ruleId = ruleId;
        this.tagType = tagType;
        this.tagValue = tagValue;
    }

    public Long getId() {
        return id;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public String getTagType() {
        return tagType;
    }

    public String getTagValue() {
        return tagValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RuleTagBean that = (RuleTagBean) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(ruleId, that.ruleId) &&
                Objects.equals(tagType, that.tagType) &&
                Objects.equals(tagValue, that.tagValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ruleId, tagType, tagValue);
    }

    @Override
    public String toString() {
        return "RuleTagBean{" +
                "id=" + id +
                ", ruleId=" + ruleId +
                ", tagType='" + tagType + '\'' +
                ", tagValue='" + tagValue + '\'' +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        private Long ruleId;
        private String tagType;
        private String tagValue;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setRuleId(Long ruleId) {
            this.ruleId = ruleId;
            return this;
        }

        public Builder setTagType(String tagType) {
            this.tagType = tagType;
            return this;
        }

        public Builder setTagValue(String tagValue) {
            this.tagValue = tagValue;
            return this;
        }

        public RuleTagBean build() {
            return new RuleTagBean(id, ruleId, tagType, tagValue);
        }
    }
}
