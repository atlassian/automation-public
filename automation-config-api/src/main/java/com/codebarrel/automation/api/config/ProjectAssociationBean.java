package com.codebarrel.automation.api.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Objects;

/**
 * Represents a project linked to a particular {@link RuleConfigBean}
 */
@JsonAutoDetect
@org.codehaus.jackson.annotate.JsonAutoDetect
public class ProjectAssociationBean {
    private String projectId;
    private String projectTypeKey;

    public ProjectAssociationBean() {
    }

    public ProjectAssociationBean(String projectId, String projectTypeKey) {
        this.projectId = projectId;
        this.projectTypeKey = projectTypeKey;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getProjectTypeKey() {
        return projectTypeKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectAssociationBean that = (ProjectAssociationBean) o;
        return Objects.equals(projectId, that.projectId) &&
                Objects.equals(projectTypeKey, that.projectTypeKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, projectTypeKey);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProjectAssociationBean{");
        sb.append("projectId='").append(projectId).append('\'');
        sb.append(", projectTypeKey='").append(projectTypeKey).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
