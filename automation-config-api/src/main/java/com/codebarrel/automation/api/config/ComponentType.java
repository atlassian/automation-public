package com.codebarrel.automation.api.config;

/**
 * Defines the different types of components an automation rule can contain.
 */
public enum ComponentType {
    /**
     * There can only be one trigger for a rule. They produce items for the conditions and actions.
     */
    TRIGGER,
    /**
     * Conditions are used to control the flow in an automation rule
     */
    CONDITION,
    /**
     * Actions perform operations when an automation rule executes
     */
    ACTION,
    /**
     * Branches control the flow of the rule and the issues that actions and conditions are applied to
     */
    BRANCH,
    /**
     * A block with a bunch of flow control if/elseif/else statements
     */
    CONDITION_BLOCK
}
