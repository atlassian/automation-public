package com.codebarrel.automation.api.thirdparty.upgrade;

import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * This is how old legacy pluggable components used to store this config. This class can be used to in upgrade tasks
 * for legacy components to deserialize the config value stored in ComponentConfigBean.getValue().
 */
@Deprecated
public class LegacyPluggableRuleConfig {
    private Map<String, List<String>> parameters = Maps.newHashMap();

    public LegacyPluggableRuleConfig() {
    }

    public LegacyPluggableRuleConfig(Map<String, List<String>> parameters) {
        this.parameters.putAll(parameters);
    }

    /**
     * Returns additional parameters to configure an action.  This could be a transition id and comment body for an
     * action that transitions an issue in Jira with a comment.
     *
     * @return a map with optional parameters to define an automation action
     */
    public Map<String, List<String>> getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PluggableRuleConfig{");
        sb.append("parameters=").append(parameters);
        sb.append('}');
        return sb.toString();
    }
}
