package com.codebarrel.automation.api.thirdparty.context;

import java.io.Serializable;

/**
 * I18n resolver that allows translation of text with params.
 */
public interface I18n {
    String getText(String key, Serializable... params);

    String getText(String key);
}
