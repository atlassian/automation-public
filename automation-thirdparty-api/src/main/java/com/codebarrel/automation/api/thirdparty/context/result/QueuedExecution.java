package com.codebarrel.automation.api.thirdparty.context.result;

import com.atlassian.jira.issue.Issue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Represents all information needed to execute the next rule component.
 */
public class QueuedExecution {
    private List<Issue> additionalIssues;
    private Map<String, Serializable> additionalInputs;

    public static QueuedExecution create(Map<String, Serializable> additionalInputs, Issue additionalIssue) {
        final List<Issue> issues = new ArrayList<>();
        issues.add(additionalIssue);
        return new QueuedExecution(additionalInputs, issues);
    }

    public static QueuedExecution create(Map<String, Serializable> additionalInputs, List<Issue> additionalIssues) {
        return new QueuedExecution(additionalInputs, additionalIssues);
    }

    public static QueuedExecution create(Map<String, Serializable> additionalInputs) {
        return new QueuedExecution(additionalInputs, null);
    }

    private QueuedExecution(Map<String, Serializable> additionalInputs, List<Issue> additionalIssues) {
        if(additionalIssues != null) {
            this.additionalIssues = new ArrayList<>();
            this.additionalIssues.addAll(additionalIssues);
        }
        this.additionalInputs = additionalInputs;
    }

    public Optional<List<Issue>> getAdditionalIssues() {
        return Optional.ofNullable(additionalIssues);
    }

    public Map<String, Serializable> getAdditionalInputs() {
        return additionalInputs;
    }
}
