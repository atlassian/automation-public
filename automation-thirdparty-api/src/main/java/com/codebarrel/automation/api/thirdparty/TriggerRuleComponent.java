package com.codebarrel.automation.api.thirdparty;

/**
 * Marker parent interface that all trigger components share.
 */
public interface TriggerRuleComponent extends AutomationRuleComponent {
}
