package com.codebarrel.automation.api.thirdparty.smartvalues;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to mark fields on classes to allow them to be rendered as smart-values.
 * <p>
 * This is to ensure that no methods with side effects are called during smart-value rendering that could cause performance/security problems.
 * (e.g. a method that causes writes to the database as a side effect)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface SmartValueSafe {
}
