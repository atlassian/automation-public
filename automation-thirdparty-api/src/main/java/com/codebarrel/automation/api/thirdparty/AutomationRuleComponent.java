package com.codebarrel.automation.api.thirdparty;

import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollection;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollectionImpl;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueContextProvider;

import java.util.Optional;

/**
 * The main interface to implement for a pluggable third party Automation for Jira rule component.
 * <p>
 * Triggers have to also implement {@link EventTriggerRuleComponent}.
 * <p>
 * Implementations of this interface will be called to validate user inputs (any configuration there might be for this component) and when
 * a rule executes.  Implementations can then control if a rule should continue executing, or stop (say if a condition didn't match).
 */
public interface AutomationRuleComponent {
    /**
     * Used to validate any configuration a user may have entered in the config UI for this component. Only implement this
     * if your component provides a config UI that requires server-side validation of the config.
     *
     * @param context The full rule context
     * @param config  The new config entered by the user
     * @return An error collection with i18nized error messages, which will be returned to the UI
     */
    default ErrorCollection validateConfig(final RuleContext context, final ComponentConfigBean config) {
        return new ErrorCollectionImpl();
    }

    /**
     * Used to provide a custom context provider to extract additional values from inputs that can be rendered
     * with smart-values.
     *
     * @return a {@link SmartValueContextProvider} or empty
     */
    default Optional<SmartValueContextProvider> getSmartValueContextProvider() {
        return Optional.empty();
    }

    /**
     * Called when this rule component should execute.  It will contain any inputs (provided by previous components in the rule chain)
     * and can control if the next components in this rule should execute via the return object.
     * <p>
     * See {@link com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult}
     *
     * @param context The full rule context
     * @param inputs  Inputs provided by previous components in the execution chain
     * @return An execution result to instruct the automation engine if execution of this rule should continue.
     */
    ExecutionResult execute(final RuleContext context, final ComponentInputs inputs);
}
