package com.codebarrel.automation.api.thirdparty.audit;

/**
 * Defines the types of associated items an audit log can have.  Different types may be rendered slightly differently
 * (e.g. the ISSUE type might be rendered as a link to the issue).
 */
public enum AuditItemAssociatedType {
    ISSUE,
    PROJECT,
    USER,
    OTHER
}
