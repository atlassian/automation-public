package com.codebarrel.automation.api.thirdparty.upgrade;

import com.codebarrel.automation.api.config.ComponentConfigBean;

/**
 * Can be implemented to upgrade the config of a particular type of third party components.
 * <p>
 * Implementations should be annotated with ExportAsService to ensure they are visible outside of their plugin.
 */
public interface ComponentUpgradeTask {

    /**
     * The complete module key of the component type to be upgraded. Should match what's in atlassian-plugin.xml
     */
    String getComponentModuleKey();

    /**
     * The schema version the component config will be upgraded to by this task.  Schema versions start at 1 by default,
     * so upgrade tasks should start from 2. Upgrade tasks will be applied in order
     */
    int getSchemaVersion();

    /**
     * A short description to log when this upgrade task runs.
     */
    String getDescription();

    /**
     * Performs the actual upgrade of a component config.
     *
     * @param oldConfig the old config currently stored in the database
     * @return the transformed config after the upgrade (or just return oldConfig, if no upgrade was performed).
     */
    ComponentUpgrade doUpgrade(ComponentConfigBean oldConfig);
}
