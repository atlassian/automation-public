package com.codebarrel.automation.api.thirdparty.upgrade;

import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.config.ComponentType;

import java.util.Objects;

/**
 * Contains all the fields that can be upgraded with new values for a {@link ComponentConfigBean} using a {@link ComponentUpgradeTask}
 */
public class ComponentUpgrade {
    private ComponentType component;
    private String type;
    private Object value;

    private ComponentUpgrade(ComponentType component, String type, Object value) {
        this.component = component;
        this.type = type;
        this.value = value;
    }

    public ComponentType getComponent() {
        return component;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComponentUpgrade{");
        sb.append("component=").append(component);
        sb.append(", type='").append(type).append('\'');
        sb.append(", value=").append(value);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentUpgrade that = (ComponentUpgrade) o;
        return getComponent() == that.getComponent() &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getValue(), that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getComponent(), getType(), getValue());
    }

    public static ComponentUpgrade.Builder upgrade(ComponentConfigBean config) {
        return new Builder().copy(config);
    }

    public static class Builder {
        private ComponentType component;
        private String type;
        private Object value;

        public Builder setComponent(ComponentType component) {
            this.component = component;
            return this;
        }


        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setValue(Object value) {
            this.value = value;
            return this;
        }

        public ComponentUpgrade build() {
            return new ComponentUpgrade(component, type, value);
        }

        private Builder copy(ComponentConfigBean config) {
            this.component = config.getComponent();
            this.type = config.getType();
            this.value = config.getValue();
            return this;
        }
    }
}
