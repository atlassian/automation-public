package com.codebarrel.automation.api.thirdparty.context;


import com.atlassian.jira.issue.Issue;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Represents the inputs. Generally these will be populated first by the trigger of a rule, then passed along to
 * subsequent components.  Subsequent components can also add extra items to these.
 */
public interface ComponentInputs {
    /**
     * Returns the initial event object that trigger the rule execution.  May be null (for scheduled rules)
     */
    String getSerializedEvent();

    /**
     * The user who initiated this rule execution. Could be empty in the case of a scheduled rule.
     */
    Optional<String> getInitiatorKey();

    /**
     * Returns an immutable list of issues (could be empty or just a single issue) introduced by earlier components
     * in the chain.
     */
    List<Issue> getIssues();

    /**
     * Map of inputs that gets passed along with the chain. This is immutable. To add more inputs for subsequent components in a rule, see methods in
     * {@link com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult}.
     * <p>
     * Please note that this map will be serialized to be persisted in the database. Type information may be lost.
     */
    Map<String, Serializable> getInputs();
}
