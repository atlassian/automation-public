package com.codebarrel.automation.api.thirdparty.context.result;

import com.atlassian.jira.issue.Issue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AdditionalInputsResultBuilder {
    private final List<QueuedExecution> queuedExecutions = new ArrayList<>();

    /**
     * Add an execution with additional inputs.  If executing in bulk, all calls to this will be smooshed together into
     * a single map containing everything.
     */
    public AdditionalInputsResultBuilder withInputs(Map<String, Serializable> additionalInputs) {
        queuedExecutions.add(QueuedExecution.create(additionalInputs));
        return this;
    }

    /**
     * Add an execution with an issue couple with some custom additional inputs.  If executing in bulk, all calls to
     * this will be smooshed together into a single map containing everything (and a list of issues).
     */
    public AdditionalInputsResultBuilder with(Map<String, Serializable> additionalInputs, Issue issue) {
        queuedExecutions.add(QueuedExecution.create(additionalInputs, issue));
        return this;
    }

    /**
     * Add an execution for a particular issue.  If executing in bulk all calls to this will be combined into a single
     * executing with set of all issues.
     */
    public AdditionalInputsResultBuilder withIssue(Issue issue) {
        this.queuedExecutions.add(QueuedExecution.create(Collections.emptyMap(), issue));
        return this;
    }

    /**
     * Add an execution for a particular issue.  If executing in bulk all calls to this will be combined into a single
     * executing with set of all issues.
     */
    public AdditionalInputsResultBuilder withIssues(List<Issue> issues) {
        issues.forEach(issue -> this.queuedExecutions.add(QueuedExecution.create(Collections.emptyMap(), issue)));
        return this;
    }

    /**
     * The preferred way to execute a rule.  This means that the rule engine will achieve higher throughput and consume
     * less memory since items queued here can be executed in parallel and each item should be smaller (e.g. single issue
     * vs multiple issues to hold in memory)
     */
    public ExecutionResult executeConcurrently() {
        return new ExecutionResult(true, queuedExecutions);
    }

    /**
     * Do not use this method unless this is absolutely necessary.  There are some uses cases where executing all
     * items makes sense, but it's strongly discouraged, since it will result in worse throughput and higher memory
     * consumption.
     * <p>
     * An example where this makes sense is, if you are adding an action that needs to process all items in bulk. For example
     * a send e-mail action will want to send a single e-mail containing details of all issues, instead of just
     * a single e-mail per issue.
     */
    public ExecutionResult executeBulk() {
        final Map<String, Serializable> combinedInputs = new HashMap<>();
        final Set<Issue> combinedIssues = new LinkedHashSet<>();

        // for bulk executions we smoosh it all together into a single execution
        queuedExecutions.forEach(queuedExecution -> {
            combinedInputs.putAll(queuedExecution.getAdditionalInputs());
            if (queuedExecution.getAdditionalIssues().isPresent()) {
                combinedIssues.addAll(queuedExecution.getAdditionalIssues().get());
            }
        });

        final List<QueuedExecution> bulkExecution = new ArrayList<>();
        if (combinedIssues.isEmpty()) {
            bulkExecution.add(QueuedExecution.create(combinedInputs));
        } else {
            bulkExecution.add(QueuedExecution.create(combinedInputs, new ArrayList<>(combinedIssues)));
        }
        return new ExecutionResult(true, bulkExecution);
    }

    /**
     * Call this method if you're unsure about executing in bulk or concurrently. For the majority of uses cases,
     * executing concurrently is what you want.
     */
    public ExecutionResult build() {
        return this.executeConcurrently();
    }
}
