package com.codebarrel.automation.api.thirdparty.context;

import java.util.List;
import java.util.Map;

/**
 * Used to return validation errors.  Errors in this collection should be i18nized.
 */
public interface ErrorCollection {
    /**
     * Return field specific errors (key being the field id)
     */
    Map<String, String> getErrors();

    /**
     * Return general error messages
     */
    List<String> getErrorMessages();

    /**
     * Returns true either if getErrors() or getErrorMessages() contain anything.
     */
    boolean hasAnyErrors();

    /**
     * Adds a new field specific error.
     */
    void addError(String key, String message);

    /**
     * Adds a new generic error message.
     */
    void addErrorMessage(String message);
}
