package com.codebarrel.automation.api.thirdparty.context.result;

import com.atlassian.jira.issue.Issue;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Builder to help components create {@link ExecutionResult}s to instruct the execution engine what
 * to do next when a rule component is done.
 * <p>
 * This builder can be used to stop, continue or continue rule execution with additional inputs.
 * <p>
 * It can also be used to queue several items (issues, or any item really) for execution either concurrently (the preferred way)
 * or in bulk if absolutely necessary.
 * <p>
 * General tips for ExecutionResults:
 * <ul>
 * <li>Do *NOT* put a big list of items into 'additionalInputs' and just continue the rule. This will be bad for performance</li>
 * <li>Instead break large lists of items down into separate 'items' since they will be processed in parallel</li>
 * </ul>
 */
public class ExecutionResultBuilder {
    private final List<QueuedExecution> queuedExecutions = new ArrayList<>();

    /**
     * Used to indicate that the rule execution should move on to the next component in the rule (with the same inputs)
     * <p>
     * E.g. when a condition check passed
     */
    public static ExecutionResult continueRule() {
        return new ExecutionResult(true, Collections.emptyList());
    }

    /**
     * Used to indicate that the rule execution should move on to the next component in the rule (with extra inputs)
     * <p>
     * E.g. an action might create an issue and put the created issue key in to the inputs so subsequent components can
     * use it.
     *
     * @param additionalInputs Extra inputs that will be available in {@link ComponentInputs} in subsequent components
     */
    public static ExecutionResult continueRuleWithAdditionalInputs(Map<String, Serializable> additionalInputs) {
        final List<QueuedExecution> newExecutions = new ArrayList<>();
        newExecutions.add(QueuedExecution.create(additionalInputs));
        return new ExecutionResult(true, newExecutions);
    }

    /**
     * Used to indicate that the rule execution should stop (e.g. if a condition check didn't pass)
     */
    public static ExecutionResult stopRule() {
        return new ExecutionResult(false, Collections.emptyList());
    }


    /**
     * @see AdditionalInputsResultBuilder#withInputs(Map)
     */
    public static AdditionalInputsResultBuilder withInputs(Map<String, Serializable> additionalInputs) {
        return new AdditionalInputsResultBuilder().withInputs(additionalInputs);
    }

    /**
     * @see AdditionalInputsResultBuilder#with(Map, Issue)
     */
    public static AdditionalInputsResultBuilder with(Map<String, Serializable> additionalInputs, Issue issue) {
        return new AdditionalInputsResultBuilder().with(additionalInputs, issue);
    }

    /**
     * @see AdditionalInputsResultBuilder#withIssue(Issue)
     */
    public static AdditionalInputsResultBuilder withIssue(Issue issue) {
        return new AdditionalInputsResultBuilder().withIssue(issue);
    }

    /**
     * @see AdditionalInputsResultBuilder#withIssues(List)
     */
    public static AdditionalInputsResultBuilder withIssues(List<Issue> issues) {
        return new AdditionalInputsResultBuilder().withIssues(issues);
    }
}
