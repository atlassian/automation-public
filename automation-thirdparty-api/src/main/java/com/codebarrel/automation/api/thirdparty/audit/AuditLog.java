package com.codebarrel.automation.api.thirdparty.audit;

/**
 * Allows {@link com.codebarrel.automation.api.thirdparty.AutomationRuleComponent}s to log messages or errors to
 * the audit log for an execution.
 */
public interface AuditLog {
    /**
     * Logs an error to the audit log. This will also change the overall state for the execution to "SOME ERRORS".
     * <p>
     * Messages must be an i18nKey (currently prefixed with 'com.codebarrel').  Currently only a single parameter is supported.
     *
     * @param i18nKey The i18n key for the audit log
     * @param param   A parameter to render after the i18n message
     * @throws IllegalArgumentException if the i18nKey doesn't begin with 'com.codebarrel', or if there's more than a single param. This restriction will be removed at some stage in future.
     */
    void addError(String i18nKey, String... param);

    /**
     * Logs a message to the audit log.
     * <p>
     * Messages must be an i18nKey (currently prefixed with 'com.codebarrel').  Currently only a single parameter is supported.
     *
     * @param i18nKey The i18n key for the audit log
     * @param param   A parameter to render after the i18n message
     * @throws IllegalArgumentException if the i18nKey doesn't begin with 'com.codebarrel', or if there's more than a single param. This restriction will be removed at some stage in future.
     */
    void addMessage(String i18nKey, String... param);

    /**
     * Adds an associated item to the audit log, that will be shown on the right hand side in the audit log.
     *
     * @param type      The type of item
     * @param itemId    The id of the item (can be null)
     * @param itemLabel The label of the item (can be null)
     * @throws IllegalArgumentException if both id and label are null
     */
    void addAssociatedItem(AuditItemAssociatedType type, String itemId, String itemLabel);
}
