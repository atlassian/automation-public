package com.codebarrel.automation.api.thirdparty.context;

import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.config.RuleConfigBean;
import com.codebarrel.automation.api.thirdparty.audit.AuditLog;

/**
 * Contains the full rule execution context
 */
public interface RuleContext {
    /**
     * The i18n resolver for the user performing an action (e.g. the user updating a rule config). This is the i18n
     * bean that should be used for validation errors.
     */
    I18n getI18n();

    /**
     * The full rule configuration including all components for this rule.  This may be needed for validation to check
     * if a rule contains other conflicting components.
     */
    RuleConfigBean getRuleConfigBean();

    /**
     * The configuration of the current component being executed or validated.
     */
    ComponentConfigBean getComponentConfigBean();

    /**
     * The AuditLog helper components can use to write audit messages during execution.
     */
    AuditLog getAuditLog();

    /**
     * Given a template string that contains smart values (e.g. {{project}}), this will return the rendered string
     * where smart-values will be substituted with real values (as provided by
     * {@link com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueContextProvider})
     */
    String renderSmartValues(String template);
}
