package com.codebarrel.automation.api.thirdparty.smartvalues;

import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;

import java.util.Map;

/**
 * Interface for adding extra properties and functions to the smart-value renderer
 */
public interface SmartValueContextProvider {

    /**
     * Returns an map where the key is smart value and the value is what it evaluates to.
     * Duplicate keys will be overridden, so take care.
     * <p>
     * Values provided in the return should make sure the getters and/or fields that
     * should be renderable are also annotated with {@link SmartValueSafe}
     *
     * @param unmodifiableContext current context. This is readonly for checking if the value exists
     * @param inputs              the current inputs object
     * @return Not null map of key values.
     */
    Map<String, Object> getContext(Map<String, Object> unmodifiableContext, ComponentInputs inputs);
}
