package com.codebarrel.automation.api.thirdparty;

import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;

import java.util.Collections;
import java.util.Set;

/**
 * Trigger implementations MUST implement this interface.  This will be used to perform
 * a fast match for any event published in Jira through the com.atlassian.event.api.EventPublisher.
 */
public interface EventTriggerRuleComponent extends TriggerRuleComponent {
    /**
     * Should return a set of all project ids that apply to the event object.  This can be an empty set if
     * no project context applies to an event (i.e. it's a GLOBAL event).
     * <p>
     * If this method returns a set of project ids, then only rules global rules, or rules restricted to those project ids
     * will run for this event. This should be a fast operation!
     *
     * @param event The event object that triggered the execution
     * @return Set of project ids, or empty set for global events
     */
    default Set<String> getProjectContext(Object event) {
        return Collections.emptySet();
    }

    /**
     * Used to convert the event object that triggered this rule to a serialized string that will be available in
     * {@link ComponentInputs#getSerializedEvent()}.  You do not have to serialize the entire object.  If further pluggable
     * components only require certain attributes from the event (e.g. a project id from a ProjectCreatedEvent), then you can
     * just return this.
     * <p>
     * This is necessary since Automation for Jira pushes all events into a database queue, so events have to be
     * serialized.  This should be a fast operation!
     *
     * @param event The event object that triggered the execution
     * @return Serialized string version of this event
     */
    String serializeEvent(Object event);

    /**
     * Returns a set of event classes to match. This should be the fully qualified event class
     * (e.g. com.atlassian.jira.event.ProjectCreatedEvent).
     * <p>
     * Implementations should ensure to make this a fast method. In busy instances this can be called a *LOT* so,
     * this method should not perform any expensive lookups.
     *
     * @return A set of event classes to match
     */
    Set<String> getHandledEventTypes();
}
