package com.codebarrel.automation.api.thirdparty.context.result;

import java.util.ArrayList;
import java.util.List;

/**
 * A result that instructs the main automation rule engine if execution should continue after the current component.
 * <p>
 * It will also contain a list of the next executions (and their inputs).  The semantics of this are that for each item
 * in the list of next executions, the automation engine will queue a new item in the main automation queue.  Queueing
 * many small items should be preferred over one large bulk item as it leads to better overall throughput and performance.
 * <p>
 * If there's just a single item queued, it will be executed inline.  See {@link ExecutionResultBuilder} for how
 * to construct these.
 *
 * @see ExecutionResultBuilder
 */
public class ExecutionResult {
    private final boolean continueRule;
    private final List<QueuedExecution> queuedExecutions = new ArrayList<>();

    ExecutionResult(boolean continueRule, List<QueuedExecution> queuedExecutions) {
        this.continueRule = continueRule;
        this.queuedExecutions.addAll(queuedExecutions);
    }

    public boolean isContinueRule() {
        return continueRule;
    }

    public List<QueuedExecution> getQueuedExecutions() {
        return queuedExecutions;
    }
}
