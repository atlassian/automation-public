package com.codebarrel.automation.api.thirdparty.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorCollectionImpl implements ErrorCollection {
    private final Map<String, String> errors = new HashMap<>();
    private final List<String> errorMessages = new ArrayList<>();

    public void addErrorMessage(String message) {
        errorMessages.add(message);
    }

    public void addError(String key, String message) {
        errors.put(key, message);
    }

    @Override
    public Map<String, String> getErrors() {
        return errors;
    }

    @Override
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public boolean hasAnyErrors() {
        return !errors.isEmpty() || !errorMessages.isEmpty();
    }
}
