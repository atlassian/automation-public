# Automation Third party extension API

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

## tl;dr

Checkout `automation-config-api` and `automation-thirdparty-api` for the latest brand new plugin APIs. `automation-api` is **DEPRECATED**!

## Content

This repository contains 3 modules:

* `automation-config-api` - defines how rule configurations and component configurations are stored. 
* `automation-thirdparty-api` - defines the API third party vendors should implement when creating extensions for Automation for Jira.
* `automation-api` - contains the outdated legacy API. **DO NOT USE**

For a working example, please see: https://bitbucket.org/atlassian/automation-addon-sample/

## Some history

In the beginning Atlassian provided the free Jira Automation Labs plugin for Jira.  This already provided an API to allow for third party vendors to provide extensions (additional actions). 

Then came Code Barrel's Automation for Jira, which was a brand new app providing simple automation for all! Eventually we talked to 
Atlassian about taking over maintenance of the dated Jira Automation Labs plugin and upgrading them to our new codebase.
 
As part of this we maintained the old Labs API, even though quite a bit of it didn't make sense any more or was confusing.

The `automation-api` contains the old legacy APIs.  This was 
quite limited however and third party vendors could only interact with issues and write actions (no conditions nor triggers).

Then more recently in July 2019 we decided to provide a more fully featured API. Some of our goals were:

* Provide third party vendors with the ability to write triggers & conditions as well
* Allow more control over the front-end (i.e. render the UI with a framework of your choosing rather than forcing React)
* Allow pluggable rule components to deal with more than just issues
* Provide access to smart-value rendering to third party apps

Thus the Automation Third Party API v3.0.0 was born, which shipped in Automation for Jira 6.0.0.  The newer 
`automation-config-api` and `automation-thirdparty-api` contains the new APIs vendors should now care about.

We'll continue to support the older legacy API for quite some time, but we highly encourage all existing extensions to move on over to the newer API.

Fast forward to 2020 and we are now owned by Atlassian again, since Code Barrel was acquired by Atlassian late lat year!

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With â¤ï¸ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)